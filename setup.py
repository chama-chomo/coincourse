import re
from pathlib import Path

from setuptools import find_packages, setup


def find_version(fname):
    """Attempt to find the version number in the file names fname.
    Raises RuntimeError if not found.
    """
    version = ""
    with open(fname, "r") as fp:
        reg = re.compile(r'__version__ = [\'"]([^\'"]*)[\'"]')
        for line in fp:
            m = reg.match(line)
            if m:
                version = m.group(1)
                break
    if not version:
        raise RuntimeError("Cannot find version information")
    return version


def requirements(fname):
    path = Path(fname)

    if not path.exists():
        return []

    return path.read_text().splitlines()


INSTALL_REQUIRES = requirements("requirements.txt")
EXTRAS_REQUIRE = {
    "test": requirements("requirements_tests.txt"),
    "docs": requirements("requirements_docs.txt"),
    "lint": requirements("requirements_lint.txt"),
}

LONG_DESCRIPTION = "\n".join(requirements("README.md"))

setup(
    name="coincourse",
    version=find_version("coincourse/__init__.py"),
    url="https://countmein.app",
    license="LGPL3",
    author="Vinnetou",
    author_email="masaj@gmx.com",
    description="CoinCourse",
    long_description=LONG_DESCRIPTION,
    install_requires=INSTALL_REQUIRES,
    packages=find_packages("coincourse"),
    package_dir={"": "coincourse"},
    extras_require=EXTRAS_REQUIRE,
    python_requires=">=3.9",
)
