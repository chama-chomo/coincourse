"""Trading strategies."""
from collections import deque
import logging
from typing import Tuple

import pandas as pd

from strategies import TradingStrategy

logger = logging.getLogger(__name__)


class SimpleStrategy(TradingStrategy):
    """Simple strategy.

    TODO describe strategy
    """

    LIMIT = 0.8
    QUEUE_LEN = 30

    def __init__(self):
        super().__init__()
        self.buy_efficiency_indicator: int = 0
        self.buy_price: int = 0
        self.efficiency_timeline: deque = deque(maxlen=self.QUEUE_LEN)

    def create_diff_column(self):
        self.stock_data["Diff"] = (
            (self.stock_data.Close - self.stock_data.Open) * (self.stock_data.Volume)
        ) + ((self.stock_data.High - self.stock_data.Open) * self.stock_data.Low)
        return self.stock_data["Diff"]

    def set_input_data(self, stock_data):
        if isinstance(stock_data, pd.DataFrame):
            self.stock_data = stock_data
            logger.debug("Stock data set / updated.")
        else:
            raise (TypeError("Stock data must be Pandas DataFrame type!"))

    def time_to_buy(self) -> Tuple[float, bool]:
        """Encourage to buy if asset falls to its minimum within a time sample."""
        self.create_diff_column()

        eff_index_avg = self.stock_data.Diff.mean()
        last_eff_index = self.stock_data.iloc[-1].Diff
        eff_max = eff_index_avg * 0.9
        eff_min = eff_index_avg * 0.5
        last_open = self.stock_data.iloc[-1].Open
        last_close = self.stock_data.iloc[-1].Close

        logger.debug(
            f"(BUY period) Asset efficiency index / curr price: {round(eff_index_avg, 3)}"
            f" / {last_close}"
        )

        self.efficiency_timeline.append(eff_index_avg)

        if not last_eff_index < eff_max and not last_eff_index > eff_min:
            logger.debug("Not enough bull power for currency..")
            self.efficiency_timeline.clear()
            return (last_open, False)

        self.buy_efficiency_indicator = eff_index_avg

        if (eff_idx := len(self.efficiency_timeline)) < self.QUEUE_LEN:
            logger.debug(f"Building efficiency index {eff_idx}/{self.QUEUE_LEN}")
            return (0, False)

        if self._get_currency_power() < self.QUEUE_LEN / 1.9:
            return (0, False)

        self.buy_price = last_open
        return (last_open, True)

    def _get_currency_power(self) -> int:
        power = self.QUEUE_LEN

        for value in self.efficiency_timeline:
            if value < self.buy_efficiency_indicator * self.LIMIT:
                power -= 1

        logger.debug(f"Crypto power is {power} out of {self.QUEUE_LEN}")
        return power

    def time_to_sell(self) -> Tuple[float, bool]:
        """Encourage to sell if asset passes certain criteria."""
        self.create_diff_column()
        eff_index_avg = self.stock_data.Diff.mean()
        current_price = self.stock_data.iloc[-1].Close
        earn_ratio = round(((current_price * 100) / self.buy_price) - 100, 2)

        logger.info(
            f"(SELL period) Asset efficiency index / ratio: {round(eff_index_avg, 3)}"
            f" / {earn_ratio}%"
        )

        self.efficiency_timeline.append(eff_index_avg)

        low_limit_price_hit: bool = current_price < (self.buy_price / 0.90)

        if round((current_price * 100) / self.buy_price) - 100 >= 0.35:
            return (self.stock_data.iloc[-1].Close, True)

        if self._get_currency_power() > self.QUEUE_LEN / 1.9:
            return (0, False)

        if current_price > self.stock_data.Close.mean():
            return (0, False)

        if not low_limit_price_hit:
            return (0, False)

        return (self.stock_data.iloc[-1].Close, True)
