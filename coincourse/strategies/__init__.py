from abc import ABC, abstractclassmethod
from typing import Tuple, Any

import numpy as np
import pandas as pd
from ta.momentum import rsi, stoch, stochrsi_d, stochrsi_k
from ta.trend import macd, macd_diff, macd_signal, SMAIndicator
from ta.volume import force_index


class TradingStrategy(ABC):
    """Abstract class for strategies."""

    def __init__(self):
        self.stock_data: pd.DataFrame = pd.DataFrame()

    @abstractclassmethod
    def set_input_data(self, stock_data: pd.DataFrame):
        pass

    @abstractclassmethod
    def time_to_buy(self) -> Tuple[float, Any, bool]:
        """Return actual price and bool representing whether to buy or not."""
        pass

    @abstractclassmethod
    def time_to_sell(self) -> Tuple[float, Any, bool]:
        """Return actual price and bool representing whether to sell or not."""
        pass


class StockIndicatorExt:
    """Class that can add new columns to original stockdata."""

    def __init__(self, stock_data):
        self.stock_data = stock_data

    def add_K_column(self):
        self.stock_data["%K"] = stoch(
            self.stock_data.High,
            self.stock_data.Low,
            self.stock_data.Close,
            window=14,
            smooth_window=3,
        )

    def add_D_column(self):
        self.stock_data["%D"] = self.stock_data["%K"].rolling(3).mean()

    def add_rsi_column(self):
        self.stock_data["RSI"] = rsi(self.stock_data.Close, window=14)

    def add_macd_column(self):
        self.stock_data["MACD"] = macd_diff(self.stock_data.Close)

    def add_stoch_rsi_d(self):
        self.stock_data["STOCHRSI_D"] = stochrsi_d(self.stock_data.Close)

    def add_stoch_rsi_k(self):
        self.stock_data["STOCHRSI_K"] = stochrsi_k(self.stock_data.Close)

    def add_force_index(self):
        self.stock_data["VOL_FORCE_IDX"] = force_index(
            self.stock_data.Close, self.stock_data.Volume
        )

    def add_sma(self):
        self.stock_data["SMA"] = SMAIndicator(close=self.stock_data.Close, window=20)

    def add_macd_diff(self):
        self.stock_data["MACD_DIFF"] = macd_diff(self.stock_data.Close)

    def add_macd(self):
        self.stock_data["MACD"] = macd(self.stock_data.Close)

    def add_macd_signal(self):
        self.stock_data["MACD_SIGNAL"] = macd_signal(self.stock_data.Close)

    def _wma(self, data, lookback):
        weights = np.arange(1, lookback + 1)
        val = data.rolling(lookback)
        wma = val.apply(lambda prices: np.dot(prices, weights) / weights.sum(), raw=True)
        return wma

    def _get_roc(self, close, n):
        difference = close.diff(n)
        nprev_values = close.shift(n)
        roc = (difference / nprev_values) * 100
        return roc

    def _get_cc(self, data, roc1_n, roc2_n, wma_lookback):
        longROC = self._get_roc(data, roc1_n)
        shortROC = self._get_roc(data, roc2_n)
        ROC = longROC + shortROC
        cc = self._wma(ROC, wma_lookback)
        return cc

    def add_coppock_curve(self):
        self.stock_data["CC"] = self._get_cc(self.stock_data.Close, 14, 11, 10)
