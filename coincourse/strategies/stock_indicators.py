"""Trading strategies."""
import logging
from collections import deque
from typing import List, Tuple, Deque

import pandas as pd
from strategies import TradingStrategy
from strategies import StockIndicatorExt


logger = logging.getLogger(__name__)


class MACDStrategy(TradingStrategy):
    """Strategy buying and selling only when RSI is above 70%.

    Better for stable coins like BTC.
    """

    def __init__(self):
        super().__init__()
        self.buy_price = 0

    def set_input_data(self, stock_data):
        if isinstance(stock_data, pd.DataFrame):
            self.stock_data = stock_data
            indicator_ext = StockIndicatorExt(self.stock_data)
            indicator_ext.add_macd()
            indicator_ext.add_macd_signal()
            indicator_ext.add_macd_diff()
            indicator_ext.add_coppock_curve()
            indicator_ext.add_sma()
            self.stock_data.dropna(inplace=True)
            logger.debug("Stock data set / updated.")
        else:
            raise (TypeError("Stock data must be Pandas DataFrame type!"))

    def time_to_buy(self):
        actual_macd = round(self.stock_data.iloc[-1].MACD, 2)
        actual_macd_signal = round(self.stock_data.iloc[-1].MACD_SIGNAL, 2)
        macd_diff_avg = round(self.stock_data.iloc[-5].MACD_DIFF.mean(), 2)
        cc_avg = round(self.stock_data.iloc[-5].CC.mean(), 2)
        cc_last = round(self.stock_data.iloc[-1].CC, 2)
        sma_decreasing = self.stock_data.SMA.tail(20).is_monotonic_decreasing
        current_time = self.stock_data.index.to_list()[:1][0]

        logger.debug(
            f"MACD/MACD_SIGNAL/DIFF(avg_50_ticks)/CC: {actual_macd}/{actual_macd_signal}/{macd_diff_avg}/{cc_avg}"
        )

        if sma_decreasing:
            logger.debug("SMA decreasing status.")
            return (self.stock_data.iloc[-1].Open, current_time, False)

        if cc_avg > 0 and cc_last > -0.25:
            logger.debug(f"Coppock Curve is too high. {cc_avg}")
            return (self.stock_data.iloc[-1].Open, current_time, False)

        if actual_macd < actual_macd_signal and actual_macd < 0:
            logger.debug("Signal higher than MACD.")
            return (self.stock_data.iloc[-1].Open, current_time, False)

        self.buy_price = self.stock_data.iloc[-1].Close
        return (self.stock_data.iloc[-1].Open, current_time, True)

    def time_to_sell(self):
        actual_macd_avg = round(self.stock_data.iloc[-5].MACD, 2)
        actual_macd_signal_avg = round(self.stock_data.iloc[-5].MACD_SIGNAL, 2)
        actual_price = self.stock_data.iloc[-1].Close
        cc_last = round(self.stock_data.iloc[-1].CC, 2)
        cc_increasing = self.stock_data.CC.tail(5).is_monotonic_increasing
        current_time = self.stock_data.index.to_list()[:1][0]

        logger.debug(
            f"MACD/MACD_SIGNAL/CC(last)/rising?: {actual_macd_avg}/{actual_macd_signal_avg}/{cc_last}/{cc_increasing})"
        )

        if cc_increasing and actual_macd_avg > actual_macd_signal_avg:
            logger.debug("CC decreasing or MACD higher than signal.")
            return (self.stock_data.iloc[-1].Close, current_time, False)

        if 0.99 * self.buy_price < actual_price < self.buy_price * 1.01:
            logger.debug("Value change too low.")
            return (self.stock_data.iloc[-1].Close, current_time, False)

        return (self.stock_data.iloc[-1].Close, current_time, True)


class StochRSIStrategy(TradingStrategy):
    """Strategy based on RSI and MACD indicators."""

    def __init__(self):
        super().__init__()
        self.last_rsi_k_average: float = 0

    @staticmethod
    def _average(dq: Deque) -> float:
        dqsum = 0
        for item in dq:
            dqsum += item
        return dqsum / len(dq)

    def set_input_data(self, stock_data):
        if isinstance(stock_data, pd.DataFrame):
            self.stock_data = stock_data
            indicator_ext = StockIndicatorExt(self.stock_data)
            indicator_ext.add_stoch_rsi_d()
            indicator_ext.add_stoch_rsi_k()
            self.stock_data.dropna(inplace=True)
            logger.debug("Stock data set / updated.")
        else:
            raise (TypeError("Stock data must be Pandas DataFrame type!"))

    def test_on_data(self):
        pass

    def time_to_buy(self) -> Tuple[float, bool]:
        """Advice to buy asset when the indicators fullfill certain criteria."""
        actual_rsi_d_avg = round(self.stock_data.iloc[-3].STOCHRSI_D.mean(), 1)
        actual_rsi_k_avg = round(self.stock_data.iloc[-3].STOCHRSI_K.mean(), 1)
        #  actual_macd = round(self.stock_data.iloc[-3].STOCHRSI_K, 2)
        print(self.stock_data.to_string())
        is_k_increasing = self.stock_data.STOCHRSI_K.is_monotonic_increasing

        logger.debug(f" RSI K / D: {actual_rsi_k_avg} / {actual_rsi_d_avg}")

        if actual_rsi_d_avg >= actual_rsi_k_avg and not is_k_increasing:
            return (self.stock_data.iloc[-1].Open, False)

        return (self.stock_data.iloc[-1].Open, True)

    def time_to_sell(self):
        """Advice to sell asset when the indicators fullfill certain criteria."""
        actual_rsi_d_avg = round(self.stock_data.iloc[-1].STOCHRSI_D, 1)
        actual_rsi_k_avg = round(self.stock_data.iloc[-1].STOCHRSI_K, 1)
        logger.debug(f" RSI K / D: {actual_rsi_k_avg} / {actual_rsi_d_avg}")

        if actual_rsi_k_avg >= actual_rsi_d_avg:
            return (self.stock_data.iloc[-1].Close, False)

        return (self.stock_data.iloc[-1].Close, True)
