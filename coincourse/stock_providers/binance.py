"""Binance Connection manager."""
import logging
import pandas as pd
from binance.client import AsyncClient, Client

logger = logging.getLogger(__name__)


class BinanceClient:
    def __init__(self, api_key: str, api_secret: str):
        self.api_key: str = api_key
        self.api_secret: str = api_secret
        self.connection: AsyncClient = None
        self.tz: str = "Europe/Prague"

    async def setup_connection(self):
        self.connection = await AsyncClient.create(self.api_key, self.api_secret)

    async def test_connection_ok(self) -> bool:
        """Testing the connection."""
        res = await self.connection.ping()
        if not res == {}:
            logger.warning(f"Connection test not OK: {res}")
            return False
        logger.debug("Connection test OK.")
        return True

    async def historical_klines_frame(self, currency: str, age: int):
        logger.debug(f"Getting historical data for: {currency}")
        try:
            klines = await self.connection.get_historical_klines(
                currency, Client.KLINE_INTERVAL_1MINUTE, f"{age} min ago UTC"
            )
            klines_frame = pd.DataFrame(klines)
            klines_frame = klines_frame.iloc[:, :6]
            klines_frame.columns = ["Time", "Open", "High", "Low", "Close", "Volume"]
            klines_frame.set_index("Time", inplace=True)
            klines_frame.index = pd.to_datetime(klines_frame.index, unit="ms")
            klines_frame.index = klines_frame.index.tz_localize(self.tz)
            klines_frame = klines_frame.astype(float)
        except Exception:
            logger.warning("Could not get data from stock provider!", exc_info=True)
            return pd.DataFrame()

        return klines_frame

    async def buy_asset_from_market(self, symbol, sum):
        logger.info(f"Making new order for MARKET BUY, sum: {sum} {symbol}")
        order = await self.connection.order_market_buy(symbol=symbol, quantity=sum)
        return order

    async def sell_asset_to_market(self, symbol, sum):
        logger.info(f"Making new order for MARKET SELL, sum: {sum} {symbol}")
        order = await self.connection.order_market_sell(symbol=symbol, quantity=sum)
        return order

    async def get_all_open_orders(self, symbol):
        orders = await self.connection.get_open_orders(symbol=symbol)
        return orders

    async def show_asset_balance(self, symbol) -> dict:
        balance = await self.connection.get_asset_balance(asset=symbol)
        return balance

    async def show_specific_order(self, symbol, id):
        result = await self.connection.get_order(symbol=symbol, orderId=id)
        return result
