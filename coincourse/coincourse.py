#!/usr/bin/env python3
import asyncio
import logging
import os
import time

from stock_providers.binance import BinanceClient
from strategies import TradingStrategy
from strategies.simple import SimpleStrategy
from strategies.stock_indicators import MACDStrategy

logging.basicConfig(
    format="%(asctime)s %(levelname)-7s [%(name)-15s] => %(message)s",
    datefmt="%H:%M:%S",
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)


async def trade1(
    sum: float,
    client: BinanceClient,
    currency: str,
    strategy: TradingStrategy,
    steps_back: int,
    sleep_time: int,
):
    order = False

    while True:
        history_sample = await client.historical_klines_frame(currency, steps_back)

        if history_sample.empty:
            time.sleep(10)
            continue

        strategy.set_input_data(history_sample)
        buy_price, buytime, ttb = strategy.time_to_buy()
        if ttb:
            print(f"*** Buying asset for {buy_price} at {buytime}! ***")
            #  order = await client.buy_asset_from_market(currency, sum)
            #  result_order = await client.show_specific_order(currency, order.get("orderId"))
            #  print(result_order)
            order = True
            break

        logger.debug("No time to buy!")
        await asyncio.sleep(sleep_time)

    while order:
        history_sample = await client.historical_klines_frame(currency, steps_back)

        if history_sample.empty:
            time.sleep(10)
            continue

        strategy.set_input_data(history_sample)
        sell_price, selltime, tts = strategy.time_to_sell()

        if tts:
            perc_diff = round(((sell_price * 100) / buy_price) - 100, 2)
            print(
                f"Selling asset for {sell_price} at {selltime}. Asset holdings changed by {perc_diff}%"
            )
            print(f"Asset change: {buy_price} > {sell_price} {currency}")

            #  order = await client.sell_asset_to_market(currency, sum)
            #  result_order = await client.show_specific_order(currency, order.get("orderId"))
            #  print(result_order)
            return perc_diff

        logger.debug("No time to sell!")
        await asyncio.sleep(sleep_time)


async def print_balance(client, symbol):
    balance = await client.show_asset_balance(symbol)
    print("=" * 30)
    print("Current Balance:")
    print(balance)
    print("=" * 30)


async def main():
    """Run entry point."""

    api_key = os.getenv("BINANCE_API_KEY")
    api_secret = os.getenv("BINANCE_API_SECRET")
    symbol = "BNB"

    if not api_key or not api_secret:
        logger.debug(f"API KEY = {api_key}")
        logger.debug(f"API SECRET = {api_secret}")
        raise RuntimeError("API secrets not provided.")

    binance_client = BinanceClient(api_key, api_secret)
    await binance_client.setup_connection()

    wallet = list()
    while await binance_client.test_connection_ok():
        await print_balance(binance_client, symbol)
        #  asset_increase = await trade1(0.00018, binance_client, "BTCUSDT", MACDStrategy(), 1000, 60)
        asset_increase = await trade1(
            0.021, binance_client, f"{symbol}USDT", MACDStrategy(), 1000, 60
        )
        wallet.append(asset_increase)
        print(f"Trades count / Wallet change: {len(wallet)}x / {sum(wallet)}%")
        print(f"Summary of trades in %: {wallet}")

    await binance_client.connection.close_connection()


if __name__ == "__main__":
    logger.info("Initializing client.")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
